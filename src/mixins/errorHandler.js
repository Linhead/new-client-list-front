import { errors } from '../consts';

export default {
  methods: {
    handleError(error) {
      let messageError = this.$t('errors.network_error');
      if (errors[error]) {
        messageError = this.$t(`errors.${errors[error]}`);
      }
      this.$message.error(messageError);
    },
  },
};
