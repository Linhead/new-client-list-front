import Vue from 'vue';
import VueRouter from 'vue-router';
import Login from '../views/Login.vue';
import Clients from '../views/Clients.vue';
import Registration from '../views/Registration.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/registration',
    name: 'Registration',
    component: Registration,
  },
  {
    path: '/',
    name: 'Clients',
    component: Clients,
    meta: { requiresAuth: true },
  },

];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    const authUser = window.localStorage.getItem('userToken');
    if (authUser) {
      next();
    } else {
      next({
        name: 'Login',
      });
    }
  } else {
    next();
  }
});

export default router;
