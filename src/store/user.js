import userApi from '@/services/user';

export default {
  namespaced: true,
  state: {
    user: null,
  },
  mutations: {
    SET_USER(state, user) {
      state.user = user;
    },
  },
  actions: {
    authUser(ctx, credentials) {
      ctx.commit('loading/TOGGLE_LOADING', true, { root: true });
      return new Promise((resolve, reject) => {
        userApi.auth(credentials).then(({data: response}) => {
          ctx.commit('SET_USER', response.user);
          localStorage.setItem('userToken', response._tokenResponse.idToken);
          localStorage.setItem('userUid', response.user.uid);
          resolve();
        }).catch(({ response }) => {
          if (response) {
            reject(response.data);
          } else {
            reject(null);
          }
        }).finally(() => {
          ctx.commit('loading/TOGGLE_LOADING', false, { root: true });
        });
      });
    },
    setUser(ctx, user) {
      ctx.commit('SET_USER', user);
    },
    getUser(ctx, uid) {
      ctx.commit('loading/TOGGLE_LOADING', true, { root: true });
      return new Promise((resolve, reject) => {
        userApi.getUser(uid).then(({ data: user }) => {
          resolve(user);
        }).catch(({ response }) => {
          if (response) {
            reject(response.data);
          } else {
            reject(null);
          }
        }).finally(() => {
          ctx.commit('loading/TOGGLE_LOADING', false, { root: true });
        });
      });
    },
    registrationUser(ctx, credentials) {
      ctx.commit('loading/TOGGLE_LOADING', true, { root: true });
      return new Promise((resolve, reject) => {
        userApi.registration(credentials).then(({ data }) => {
          ctx.commit('SET_USER', data.user);
          localStorage.setItem('userToken', data._tokenResponse.idToken);
          localStorage.setItem('userUid', data.user.uid);
          resolve();
        }).catch(({ response }) => {
          if (response) {
            reject(response.data);
          } else {
            reject(null);
          }
        }).finally(() => {
          ctx.commit('loading/TOGGLE_LOADING', false, { root: true });
        });
      });
    },
    recoverPassword(ctx, email) {
      ctx.commit('loading/TOGGLE_LOADING', true, { root: true });
      return new Promise((resolve, reject) => {
        userApi.recoverPassword(email).then(() => {
          resolve();
        }).catch(({ response }) => {
          if (response) {
            reject(response.data);
          } else {
            reject(null);
          }
        }).finally(() => {
          ctx.commit('loading/TOGGLE_LOADING', false, { root: true });
        });
      });
    },
  },
};
