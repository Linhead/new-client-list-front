import clientsApi from '@/services/clients';

export default {
  namespaced: true,
  state: {
    limit: 5,
  },
  mutations: {
    SET_LIMIT(state, limit) {
      state.limit = limit;
    },
  },
  actions: {
    setLimit(ctx, limit) {
      localStorage.setItem('limitNumber', limit);
      ctx.commit('SET_LIMIT', limit);
    },
    getClients(ctx) {
      ctx.commit('loading/TOGGLE_LOADING', true, { root: true });
      return new Promise((resolve, reject) => {
        clientsApi.getClients()
        .then(({data: clients}) => {
          resolve(clients);
        })
        .catch(({ response }) => {
          if (response) {
            reject(response.data);
          } else {
            reject(null);
          }
        })
        .finally(() => {
          ctx.commit('loading/TOGGLE_LOADING', false, { root: true });
        });
      });
    },
  },
};