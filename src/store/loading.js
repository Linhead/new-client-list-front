export default {
  namespaced: true,
  state: {
    loading: false,
  },
  mutations: {
    TOGGLE_LOADING(state, loading) {
      state.loading = loading;
    },
  },
};
