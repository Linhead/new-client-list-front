import axios from 'axios';
import { baseFetchURL } from '../consts';

const apiClients = axios.create({
  baseURL: baseFetchURL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 10000,
});

export default {
  getClients() {
    return apiClients.get('/getClients');
  },
};