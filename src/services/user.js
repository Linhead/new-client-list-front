import axios from 'axios';
import { baseFetchURL } from '../consts';

const apiUser = axios.create({
  baseURL: baseFetchURL,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
  timeout: 10000,
});

export default {
  auth(credentials) {
    return apiUser.post('/login', credentials);
  },
  registration(credentials) {
    return apiUser.post('/registration', credentials);
  },
  recoverPassword(email) {
    return apiUser.post('/recoverPassword', email);
  },
  getUser(uid) {
    return apiUser.get('/getUser', {
      params: {
        uid,
      },
    });
  },
};
