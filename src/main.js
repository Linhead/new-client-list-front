import Vue from 'vue';
import Vuelidate from 'vuelidate';
import ElementUI from 'element-ui';
import VueI18n from 'vue-i18n';
import App from './App.vue';
import router from './router';
import store from './store';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(Vuelidate);
Vue.use(ElementUI);
Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: 'ru',
  fallbackLocale: 'ru',
  messages: {
    ru: { ...require('./i18n/ru.json') },
    en: { ...require('./i18n/en.json') },
  },
});

Vue.filter('formatDate', function(val) {
  let date = new Date(val);
  return date.toLocaleDateString("ru-RU", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: (h) => h(App),
}).$mount('#app');
