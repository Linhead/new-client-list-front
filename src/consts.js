const baseFetchURL = 'https://client-list-back.herokuapp.com/';
// 'http://localhost:3000';

const errors = {
  'auth/wrong-password': 'wrong_password',
  'auth/email-already-in-use': 'email_already_in_use',
  'auth/user-not-found': 'user_not_found',
  'auth/missing-email': 'email_not_found',
};

export { baseFetchURL, errors };
