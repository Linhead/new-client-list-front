# New-client-list-front
https://linhead.gitlab.io/new-client-list-front

My pity pet-project with Vue and Vuex (in future gonna add TypeScript and Nuxt). 

It's based on my old same projects (https://gitlab.com/Linhead/clients-list-on-vue) but deeply refactored. 

It's on development stage.

Expected output - web app which shows table with company's clients with possibility to manage them (add/remove/edit etc.).  

The backend part - https://gitlab.com/Linhead/new-client-list-back
